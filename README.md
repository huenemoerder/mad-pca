# coMAD - SISAP 2019

# Requirements

- [numpy](https://www.numpy.org/) 
- [pandas](https://pandas.pydata.org/)
- [matplotlib](https://matplotlib.org/)
- [sklearn](https://scikit-learn.org/stable/)